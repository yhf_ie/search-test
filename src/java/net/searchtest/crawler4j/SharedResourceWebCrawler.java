package net.searchtest.crawler4j;

import edu.uci.ics.crawler4j.crawler.WebCrawler;

/**
 * <p>
 * A <code>WebCrawler</code> that accepts a shared resource among all the
 * instances of this crawler
 * </p>
 * <p>
 * please note that the shared resource will be accessed from different threads
 * at the same time.
 * </p>
 * 
 * @author yahya
 * @see WebCrawler
 *
 */
public class SharedResourceWebCrawler<S> extends WebCrawler {
	private S sharedResource;

	public S getSharedResource() {
		return sharedResource;
	}

	public void setSharedResource(S sharedResource) {
		this.sharedResource = sharedResource;
	}

}
