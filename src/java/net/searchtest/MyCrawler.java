package net.searchtest;

import java.util.regex.Pattern;

import net.searchtest.crawler4j.SharedResourceWebCrawler;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class MyCrawler extends SharedResourceWebCrawler<MyCrawlerConfig> {

	private final static Pattern notAllowed = Pattern
			.compile(".*(\\.(css|js|gif|jpg||png|jpeg|bmp|mp3|mp3|zip|gz))$");

	public MyCrawler() {
		super();
	}

	@Override
	public boolean shouldVisit(Page page, WebURL url) {
		String testedUrl = url.getURL().toLowerCase();

		return shouldVisit(testedUrl);
	}

	private boolean shouldVisit(String url) {
		return !notAllowed.matcher(url).matches()
				&& url.startsWith(getSharedResource().getSearchDomain());
	}

	@Override
	public void visit(Page page) {

		if (page.getParseData() instanceof HtmlParseData) {

			CrawlResults results = getSharedResource().getResults();
			String currentUrl = page.getWebURL().getURL();
			getSharedResource().checkHomePage(currentUrl);

			if (currentUrl.equals(getSharedResource().getHomePageUrl())) {

				HtmlParseData p = (HtmlParseData) page.getParseData();
				int topLevelPages = 0;
				for (WebURL u : p.getOutgoingUrls()) {
					if (shouldVisit(u.getURL())) {
						topLevelPages++;
					}
				}
				results.setFirstPageLinks(topLevelPages);

			}

			results.addUrl(page.getWebURL().getURL());
			results.increaseVisitedPages();

		}

	}
}
