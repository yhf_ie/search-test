package net.searchtest;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import net.searchtest.crawler4j.SharedResourceCrawlController;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class CrawlerInitializer {

	private SharedResourceCrawlController controller;
	private static final int crawlerCount = 1;
	public static final File CRAWLER_STORAGE_PARENT_DIR = new File(
			"crawlerdata");

	private final File crawlerStorageFile = new File(
			CRAWLER_STORAGE_PARENT_DIR, UUID.randomUUID().toString());

	// final String CRAWLER_STORAGE = "/home/yahya/crawler/root";

	private CrawlConfig cfg;
	private PageFetcher fetcher;
	private RobotstxtConfig rCfg;
	private RobotstxtServer rServer;

	public CrawlerInitializer() throws IOException {
		System.out.println("Initializing crawler.......");
		System.out.println("crawler storage file is: "
				+ crawlerStorageFile.getPath());
		cfg = new CrawlConfig();
		cfg.setCrawlStorageFolder(crawlerStorageFile.getPath());
		cfg.setMaxPagesToFetch(-1);
		// cfg.setMaxDepthOfCrawling(2);
		cfg.setPolitenessDelay(1000);
		// cfg.setResumableCrawling(true);

		fetcher = new PageFetcher(cfg);
		rCfg = new RobotstxtConfig();
		rServer = new RobotstxtServer(rCfg, fetcher);

	}

	private void crawl(boolean async, MyCrawlerConfig crawlerConfig) {
		try {
			controller = new SharedResourceCrawlController(cfg, fetcher,
					rServer);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		controller.addSeed(crawlerConfig.getSearchDomain());
		if (async) {
			controller.startNonBlocking(MyCrawler.class, crawlerCount,
					crawlerConfig);
		} else {
			controller.start(MyCrawler.class, crawlerCount, crawlerConfig);
		}
		System.out.println("Finished initializing.....");
	}

	public void crawl(MyCrawlerConfig crawlerConfig) {
		this.crawl(true, crawlerConfig);
	}

	public void crawl(MyCrawlerConfig crawlerConfig, boolean async) {
		this.crawl(async, crawlerConfig);
	}

	public void dispose() {
		System.out.println("Destroying crawler......");
		controller.shutdown();
		// controller.waitUntilFinish();
		crawlerStorageFile.delete();
		System.out.println("Finished shutdown.....");
	}

	public CrawlController getController() {
		return controller;
	}

	public File getCrawlerStorageFile() {
		return crawlerStorageFile;
	}

}
