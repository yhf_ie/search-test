package net.searchtest;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CrawlResults implements Serializable {

	private static final long serialVersionUID = 1L;
	private int totalVisitedPages;
	private int firstPageLinks;
	private List<String> totalUrls = Collections
			.synchronizedList(new LinkedList<String>());

	public synchronized int getTotalVisitedPages() {
		return totalVisitedPages;
	}

	public synchronized int getFirstPageLinks() {
		return firstPageLinks;
	}

	public synchronized void setFirstPageLinks(int firstPageLinks) {
		this.firstPageLinks = firstPageLinks;
	}

	public synchronized void increaseVisitedPages() {
		this.totalVisitedPages++;
	}

	public void addUrl(String url) {
		totalUrls.add(url);
	}

	public List<String> getTotalUrls() {
		return totalUrls;
	}

}
