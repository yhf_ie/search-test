<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<title>Insert title here</title>
</head>
<body>
	<%--	<g:javascript library="jquery" />--%>
	<g:javascript>
		setInterval(function(){
			${remoteFunction(controller: "results", action: "results", update: [success: 'resultsDiv']) }
		}, 3000);
	</g:javascript>
	<div class="body">
		<div id="resultsDiv"></div>
	</div>
</body>
</html>