package net.searchtest;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

public class CrawlerTest {
	@Test
	@Ignore
	public void testCrawler() throws Exception {

		SearchDomainHolder.setSearchDomain("http://www.ics.uci.edu/");
		CrawlerInitializer b = new CrawlerInitializer();
		b.crawl(null);
		List<Object> data = b.getController().getCrawlersLocalData();

		for (Object d : data) {

			if (d != null && (d instanceof CrawlResults)) {
				CrawlResults stats = (CrawlResults) d;

				System.out.println("total visited pages: "
						+ stats.getTotalVisitedPages());
				System.out.println("firstPageLinks: "
						+ stats.getFirstPageLinks());
			}

		}
		b.dispose();
	}
}
