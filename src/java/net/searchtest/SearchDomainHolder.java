package net.searchtest;

public class SearchDomainHolder {

	private static String searchDomain;

	public static String getSearchDomain() {
		return searchDomain;
	}

	public static void setSearchDomain(String searchDomain) {
		SearchDomainHolder.searchDomain = searchDomain;
	}

}
