package net.searchtest;

public class MyCrawlerConfig {

	private String searchDomain;
	private CrawlResults results;
	private String homePageUrl;

	public String getSearchDomain() {
		return searchDomain;
	}

	public void setSearchDomain(String searchDomain) {
		this.searchDomain = searchDomain;
	}

	public CrawlResults getResults() {
		return results;
	}

	public void setResults(CrawlResults results) {
		this.results = results;
	}

	public synchronized String getHomePageUrl() {
		return homePageUrl;
	}

	public synchronized void checkHomePage(String pageUrl) {
		if (homePageUrl == null) {
			homePageUrl = pageUrl;
		}
	}
}
