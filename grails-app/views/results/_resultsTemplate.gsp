<p>
	<g:if test="${finished}">
Done!
</g:if>
	<g:if test="${!finished}">
Searching...
</g:if>
</p>
<p>
	Total visited pages are:
	${results?.totalVisitedPages}
</p>
<p>
	First page links count:
	${results?.firstPageLinks}
</p>
<h1>Scanned URLs are:</h1>
<ul>
	<g:each in="${results?.totalUrls}" var="${res}">
		<li>
			${res}
		</li>
	</g:each>
</ul>
