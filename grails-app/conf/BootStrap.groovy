import net.searchtest.CrawlerShutdownHook


class BootStrap {

    def init = { servletContext ->
		Runtime.runtime.addShutdownHook(new CrawlerShutdownHook())
    }
    def destroy = {
    }
}
