package net.searchtest;

import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class CrawlerShutdownHook extends Thread {

	@Override
	public void run() {
		super.run();
		System.out.println("Shutdown hook was called...");
		try {
			FileUtils
					.deleteDirectory(CrawlerInitializer.CRAWLER_STORAGE_PARENT_DIR);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
