package search.test

import org.h2.command.ddl.CreateLinkedTable;

import net.searchtest.CrawlResults
import net.searchtest.CrawlerInitializer;
import net.searchtest.MyCrawlerConfig;
import net.searchtest.SearchDomainHolder;

class ResultsController {

	def index() {

		String searchDomain=params.search

		if(searchDomain){
			session.ci?.dispose()
			session.config=new MyCrawlerConfig()
			session.config.results=new CrawlResults()
			session.config.searchDomain=searchDomain
			session.ci=new CrawlerInitializer()
			session.ci.crawl(session.config);
		}
	}

	def results(){

		if(session.ci){
			render(template: 'resultsTemplate', model: [results: session.config?.results, finished: session.ci?.controller?.finished])
		}else{
			def url=createLink(uri: '/')
			render(contentType: 'text/html', text: "<script>window.location.href='$url'</script>")
		}
	}
}
